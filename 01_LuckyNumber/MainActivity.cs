﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System;

namespace _01_LuckyNumber
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true, Icon = "@mipmap/new_res")]
    public class MainActivity : AppCompatActivity
    {
        // define iur variables;
        SeekBar seekBar;
        TextView resultTextView;
        Button rollButton;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            seekBar = FindViewById<SeekBar>(Resource.Id.seekBar);
            resultTextView = FindViewById<TextView>(Resource.Id.resultTextView);
            rollButton = (Button)FindViewById(Resource.Id.rollButton);
            SupportActionBar.Title = "My Lucky Number App";

            // resultTextView.Text = "9";
            rollButton.Click += RollButton_Click;
        }

        private void RollButton_Click(object sender, System.EventArgs e)
        {
            Random random = new Random();
            int luckyNumber = random.Next(seekBar.Progress) + 1; // using +1 to remove 0 from display
            resultTextView.Text = luckyNumber.ToString();
        }
    }
}