﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using _04_QuizApp.Helpers;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace _04_QuizApp.Activities
{
    [Activity(Label = "QuizDescriptionActivity", Theme = "@style/AppTheme")]
    public class QuizDescriptionActivity : AppCompatActivity
    {
        TextView quizTopicTextView;
        TextView quizDescriptionTextView;
        ImageView quizImageView;
        Button startQuizButtonView;

        // variables custom
        string quizTopic;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            // selected_topics is html template for page
            SetContentView(Resource.Layout.selected_topics);

            quizTopicTextView = (TextView)FindViewById(Resource.Id.quizTopicText);
            quizDescriptionTextView = (TextView)FindViewById(Resource.Id.quizDescriptionText);
            quizImageView = (ImageView)FindViewById(Resource.Id.quizImage);
            startQuizButtonView = (Button)FindViewById(Resource.Id.startQuizButton);

            // get value from intended sended from main page
            quizTopic = Intent.GetStringExtra("topic");
            quizTopicTextView.Text = quizTopic;

            quizImageView.SetImageResource(GetImage(quizTopic));

            // get description of topic
            QuizHelper quizHelper = new QuizHelper();
            quizDescriptionTextView.Text = quizHelper.GetTopicDescription(quizTopic);

            // write this shit for redirect from quiz pages
            startQuizButtonView.Click += StartQuizButtonView_Click;

        }

        private void StartQuizButtonView_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(QuizActivity));
            intent.PutExtra("topic", quizTopic);
            StartActivity(intent);
            Finish(); // close this activity
        }

        int GetImage(string topic)
        {
            int imageInt = 0;
            if(topic == "History")
            {
                imageInt = Resource.Drawable.history;
            }
            else if(topic == "Geography")
            {
                imageInt = Resource.Drawable.geography;
            }
            else if (topic == "Space")
            {
                imageInt = Resource.Drawable.space;
            }
            else if (topic == "Programming")
            {
                imageInt = Resource.Drawable.programming;
            }
            else if (topic == "Business")
            {
                imageInt = Resource.Drawable.business;
            }
            else if (topic == "Engineering")
            {
                imageInt = Resource.Drawable.engineering;
            }

            return imageInt;
        }
    }
}