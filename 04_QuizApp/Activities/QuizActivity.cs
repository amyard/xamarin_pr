﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using _04_QuizApp.Fragments;
using _04_QuizApp.Helpers;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Quiz_App.DataModels;

namespace _04_QuizApp.Activities
{
    [Activity(Label = "QuizActivity")]
    public class QuizActivity : AppCompatActivity
    {
        Android.Support.V7.Widget.Toolbar toolbar;

        RadioButton optionARadio, optionBRadio, optionCRadio, optionDRadio;
        TextView optionATextView, optionBTextView, optionCTextView, optionDTextView, quizPositionTextView, questionTextView, timeCounterTextView;
        Button proceedQuizButton;

        List<Question> quizQuestionList = new List<Question>();
        QuizHelper quizHelper = new QuizHelper();  
        int quizPosition;

        string quizTopic;

        double correctAnswerCount = 0;

        int timeCounter = 0;
        DateTime dateTime;
        System.Timers.Timer countDown = new System.Timers.Timer();
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.quiz_page);

            quizTopic = Intent.GetStringExtra("topic");

            toolbar = (Android.Support.V7.Widget.Toolbar)FindViewById(Resource.Id.quizToolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = quizTopic + " Quiz";

            // set return bar arrow
            Android.Support.V7.App.ActionBar actionBar = SupportActionBar;
            actionBar.SetHomeAsUpIndicator(Resource.Drawable.outline_arrowback);
            actionBar.SetDisplayHomeAsUpEnabled(true);

            ConnectViews();
            BeginQuiz();

            // countdown 1 sec - every tick = 1s
            countDown.Interval = 1000;
            countDown.Elapsed += CountDown_Elapsed;
        }

        private void CountDown_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timeCounter++;

            DateTime dt = new DateTime();
            dt = dateTime.AddSeconds(-1);

            var dateDifference = dateTime.Subtract(dt);
            dateTime = dateTime - dateDifference;

            RunOnUiThread(() =>
            {
                timeCounterTextView.Text = dateTime.ToString("mm:ss");
            });

            // end quiz on timeout
            if(timeCounter == 120)
            {
                countDown.Enabled = false;
                CompletQuiz();
            }
        }


        // for quiz
        void ConnectViews()
        {
            optionARadio = (RadioButton)FindViewById(Resource.Id.optionARadio);
            optionBRadio = (RadioButton)FindViewById(Resource.Id.optionBRadio);
            optionCRadio = (RadioButton)FindViewById(Resource.Id.optionCRadio);
            optionDRadio = (RadioButton)FindViewById(Resource.Id.optionDRadio);

            optionARadio.Click += OptionARadio_Click;
            optionBRadio.Click += OptionBRadio_Click;
            optionCRadio.Click += OptionCRadio_Click;
            optionDRadio.Click += OptionDRadio_Click;

            optionATextView = (TextView)FindViewById(Resource.Id.optionATextView);
            optionATextView = (TextView)FindViewById(Resource.Id.optionATextView);
            optionBTextView = (TextView)FindViewById(Resource.Id.optionBTextView);
            optionCTextView = (TextView)FindViewById(Resource.Id.optionCTextView);
            optionDTextView = (TextView)FindViewById(Resource.Id.optionDTextView);

            questionTextView = (TextView)FindViewById(Resource.Id.questionTextView);
            quizPositionTextView = (TextView)FindViewById(Resource.Id.quizPositionTextView);
            timeCounterTextView = (TextView)FindViewById(Resource.Id.timeCounterTextView);

            proceedQuizButton = (Button)FindViewById(Resource.Id.proceedQuizButton);
            proceedQuizButton.Click += ProceedQuizButton_Click;

        }

        private void ProceedQuizButton_Click(object sender, EventArgs e)
        {
            // validation - nothing checked
            if(!optionARadio.Checked && !optionBRadio.Checked && !optionCRadio.Checked && !optionDRadio.Checked)
            {
                // Toast.MakeText(this, "Please, choose the answer!!!", ToastLength.Short).Show();
                Snackbar.Make((View)sender, "Please, choose the answer!!!", Snackbar.LengthShort).Show();
            }
            // choose  correct answer
            else if(optionARadio.Checked)
            {
                if(optionATextView.Text == quizQuestionList[quizPosition - 1].Answer)
                {
                    // display Defaul message
                    // Toast.MakeText(this, "The answer is correct", ToastLength.Short).Show();
                    correctAnswerCount++;
                    CorrectAnswer();
                } 
                else
                {
                    // display Defaul message
                    // Toast.MakeText(this, "Incorrect answer", ToastLength.Short).Show();
                    IncorrectAnswer();
                }
            }
            else if (optionBRadio.Checked)
            {
                if (optionBTextView.Text == quizQuestionList[quizPosition - 1].Answer)
                {
                    // display Defaul message
                    // Toast.MakeText(this, "The answer is correct", ToastLength.Short).Show();
                    correctAnswerCount++;
                    CorrectAnswer();
                }
                else
                {
                    // display Defaul message
                    // Toast.MakeText(this, "Incorrect answer", ToastLength.Short).Show();
                    IncorrectAnswer();
                }
            }
            else if (optionCRadio.Checked)
            {
                if (optionCTextView.Text == quizQuestionList[quizPosition - 1].Answer)
                {
                    // display Defaul message
                    // Toast.MakeText(this, "The answer is correct", ToastLength.Short).Show();
                    correctAnswerCount++;
                    CorrectAnswer();
                }
                else
                {
                    // display Defaul message
                    // Toast.MakeText(this, "Incorrect answer", ToastLength.Short).Show();
                    IncorrectAnswer();
                }
            }
            else if (optionDRadio.Checked)
            {
                if (optionDTextView.Text == quizQuestionList[quizPosition - 1].Answer)
                {
                    // display Defaul message
                    // Toast.MakeText(this, "The answer is correct", ToastLength.Short).Show();
                    correctAnswerCount++;
                    CorrectAnswer();
                }
                else
                {
                    // display Defaul message
                    // Toast.MakeText(this, "Incorrect answer", ToastLength.Short).Show();
                    IncorrectAnswer();
                }
            }
        }

        private void OptionDRadio_Click(object sender, EventArgs e)
        {
            ClearOptionSelected();
            optionDRadio.Checked = true;
        }

        private void OptionCRadio_Click(object sender, EventArgs e)
        {
            ClearOptionSelected();
            optionCRadio.Checked = true;
        }

        private void OptionBRadio_Click(object sender, EventArgs e)
        {
            ClearOptionSelected();
            optionBRadio.Checked = true;
        }

        private void OptionARadio_Click(object sender, EventArgs e)
        {
            ClearOptionSelected();
            optionARadio.Checked = true;
        }

        void ClearOptionSelected()
        {
            optionARadio.Checked = false;
            optionBRadio.Checked = false;
            optionCRadio.Checked = false;
            optionDRadio.Checked = false;
        }

        // get quiz
        void BeginQuiz()
        {
            quizPosition = 1;
            quizQuestionList = quizHelper.GetQuizQuestions(quizTopic);
            questionTextView.Text = quizQuestionList[0].QuizQuestion;
            optionATextView.Text = quizQuestionList[0].OptionA;
            optionBTextView.Text = quizQuestionList[0].OptionB;
            optionCTextView.Text = quizQuestionList[0].OptionC;
            optionDTextView.Text = quizQuestionList[0].OptionD;

            quizPositionTextView.Text = "Quiestion " + quizPosition.ToString() + "/" + quizQuestionList.Count();

            // counter down
            dateTime = new DateTime();
            dateTime = dateTime.AddMinutes(2);
            timeCounterTextView.Text = dateTime.ToString("mm:ss");

            countDown.Enabled = true;
        }

        void CorrectAnswer()
        {
            CorrectFragment correctFragment = new CorrectFragment();
            var trans = SupportFragmentManager.BeginTransaction();

            // will nor dismiss automatically
            correctFragment.Cancelable = false;
            correctFragment.Show(trans, "Correct");
            correctFragment.NextQuestion += CorrectFragment_NextQuestion;
        }

        void IncorrectAnswer()
        {
            IncorrectFragment incorrectFragment = new IncorrectFragment(quizQuestionList[quizPosition -1].Answer);
            var trans = SupportFragmentManager.BeginTransaction();
            incorrectFragment.Cancelable = false;
            incorrectFragment.Show(trans, "Incorrect");
            incorrectFragment.NextQuestion += CorrectFragment_NextQuestion;
        }

        private void CorrectFragment_NextQuestion(object sender, EventArgs e)
        {
            // next question
            quizPosition++;

            // if no more question
            if(quizPosition > quizQuestionList.Count)
            {
                // Toast.MakeText(this, "No more quetions", ToastLength.Short).Show();
                CompletQuiz();
                return;
            }

            int indx = quizPosition - 1;
            ClearOptionSelected();

            questionTextView.Text = quizQuestionList[indx].QuizQuestion;
            optionATextView.Text = quizQuestionList[indx].OptionA;
            optionBTextView.Text = quizQuestionList[indx].OptionB;
            optionCTextView.Text = quizQuestionList[indx].OptionC;
            optionDTextView.Text = quizQuestionList[indx].OptionD;

            quizPositionTextView.Text = "Question " + quizPosition.ToString() + "/" + quizQuestionList.Count.ToString();
        }

        void CompletQuiz()
        {
            timeCounterTextView.Text = "00:00";
            countDown.Enabled = false;

            string score = correctAnswerCount.ToString() + "/" + quizQuestionList.Count.ToString();
            double percentage = (correctAnswerCount / double.Parse(quizQuestionList.Count.ToString())) * 100;
            string remarks = "";
            string image = "";

            if(percentage > 50 && percentage < 70)
            {
                remarks = "Very Good Results, you\nReally tried";
            }
            else if(percentage >= 70)
            {
                remarks = "Very outstaffing result, you\nkilled it";
            }
            else if (percentage == 70)
            {
                remarks = "Not bad, average result";
            }
            else
            {
                remarks = "Stupid asshole";
                image = "failed";
            }

            CompletedFragment completedFragment = new CompletedFragment(remarks, score, image);
            completedFragment.Cancelable = false;

            var trans = SupportFragmentManager.BeginTransaction();
            completedFragment.Show(trans, "Complete");

            completedFragment.GoHome += (sender, e) =>
            {
                this.Finish();
            };
        }
    }
}