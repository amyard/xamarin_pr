﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;

namespace _02_IncommePlanner
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        EditText incomePerHourEditText;
        EditText workHoursEditText;
        EditText taxRateEditText;
        EditText savingsRateEditText;

        TextView workSummaryTextView;
        TextView grossIncomeTextView;
        TextView taxPayableTextView;
        TextView annualSavingsTextView;
        TextView spendableIncomesTextView;

        Button calculateButton;
        RelativeLayout resultLayout;

        bool inputCalculated = false; // for clean input class

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            ConnectViews();
        }

        void ConnectViews()
        {
            incomePerHourEditText = FindViewById<EditText>(Resource.Id.incomePerHourEditText);
            workHoursEditText = (EditText)FindViewById(Resource.Id.workHoursEditText);
            taxRateEditText = (EditText)FindViewById(Resource.Id.taxRateEditText);
            savingsRateEditText = (EditText)FindViewById(Resource.Id.savingsRateEditText);


            workSummaryTextView = (TextView)FindViewById(Resource.Id.workSummaryTextView);
            grossIncomeTextView = (TextView)FindViewById(Resource.Id.grossIncomeTextView);
            taxPayableTextView = (TextView)FindViewById(Resource.Id.taxPayableTextView);
            annualSavingsTextView = (TextView)FindViewById(Resource.Id.annualSavingsTextView);
            spendableIncomesTextView = (TextView)FindViewById(Resource.Id.spendableIncomesTextView);


            calculateButton = (Button)FindViewById(Resource.Id.calculateButton);
            resultLayout = (RelativeLayout)FindViewById(Resource.Id.resultLayout);

            calculateButton.Click += CalculateButton_Click;
        }

        private void CalculateButton_Click(object sender, System.EventArgs e)
        {

            if (inputCalculated)
            {
                inputCalculated = false;
                calculateButton.Text = "Calculate";
                ClearInput();
                return;
            }

            // Take input from user
            double incomePerHour = double.Parse(incomePerHourEditText.Text);
            double workHourPerDay = double.Parse(workHoursEditText.Text);
            double taxRate = double.Parse(taxRateEditText.Text);
            double savingsRate = double.Parse(savingsRateEditText.Text);

            // where are 52 weeks per year, but 2 weeks spand on vacation
            double annualWorkHoursSummary = workHourPerDay * 5 * 50;
            double annualIncome = incomePerHour * workHourPerDay * 5 * 50;
            double taxPayable = (taxRate / 100) * annualIncome;
            double annualSavings = (savingsRate / 100) * annualIncome;
            double spendableIncome = annualIncome - annualSavings - taxPayable;

            // display the result of the calculation
            // "#,##"  - add come after 1000
            grossIncomeTextView.Text = annualIncome.ToString("#,##") + " HRS";
            workSummaryTextView.Text = annualWorkHoursSummary.ToString("#,##") + " USD";
            taxPayableTextView.Text = taxPayable.ToString("#,##") + " USD";
            annualSavingsTextView.Text = annualSavings.ToString("#,##") + " USD";
            spendableIncomesTextView.Text = spendableIncome.ToString("#,##") + " USD";

            // display section with our new data
            resultLayout.Visibility = Android.Views.ViewStates.Visible;

            inputCalculated = true; // for clean input boolean. if true - clear inputs
            calculateButton.Text = "Clear";
        }

        void ClearInput()
        {
            incomePerHourEditText.Text = "";
            workHoursEditText.Text = "";
            taxRateEditText.Text = "";
            savingsRateEditText.Text = "";

            resultLayout.Visibility = Android.Views.ViewStates.Invisible;
        }
    }
}